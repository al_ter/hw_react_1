import React from 'react';

const MessageFeatures = ({ messageId, onDelete, onEdit }) => {
  return (
    <div className='chat-message__features'>
      <span className='edit' onClick={() => onEdit(messageId)}>
        <i className='bi bi-pencil' />
        <i className='bi bi-pencil-fill' />
      </span>
      <span className='delete' onClick={() => onDelete(messageId)}>
        <i className='bi bi-trash' />
        <i className='bi bi-trash-fill' />
      </span>
    </div>
  );
};

export default MessageFeatures;
