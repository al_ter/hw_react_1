import React from 'react';
import MessageFeatures from './MessageFeatures';
import Divider from '../Divider';

const Message = ({ message, currentUserId, onLike, onDelete, onEdit, dividerTimestamp }) => {
  const { id, userId, avatar, user, text, likeCount = 0 } = message;
  const isSelf = userId === currentUserId;
  const messagePosition = isSelf ? 'right' : 'left';
  const createdAt = new Date(message.createdAt);
  const editedAt = new Date(message.editedAt);
  const createdAtLocale = createdAt.toLocaleString();
  const editedAtLocale = editedAt.toLocaleString();
  const createdAtTime = createdAt.toLocaleTimeString();
  const editedAtTime = editedAt.toLocaleTimeString();
  const time = message.editedAt ? `edited ${editedAtTime}` : createdAtTime;
  const fullTime = message.editedAt ? editedAtLocale : createdAtLocale;

  return (
    <>
      {
        dividerTimestamp && <Divider dividerTimestamp={dividerTimestamp} />
      }

      <div className={`chat-message-${messagePosition} pb-4`}>
        <div>
          {
            !isSelf && <img
              src={avatar}
              className='rounded-circle mr-1'
              alt={user}
              width='40'
              height='40'
            />
          }
        </div>
        <div className='chat-message__text flex-shrink-1 bg-light rounded py-2 px-3'>
          {
            !isSelf &&
            <div className='font-weight-bold mb-1'>
              {user}
            </div>
          }

          {text}

          <div className='chat-message__bottom d-flex text-muted justify-content-between'>
            <span className='small text-nowrap mt-2' title={fullTime}>
              {time}
            </span>

            {
              <span className={`like mt-1 ${likeCount > 0 && 'liked'}`} onClick={() => !isSelf && onLike(id)}>
                <i className='bi bi-hand-thumbs-up' />
                <i className='bi bi-hand-thumbs-up-fill' /> {likeCount > 0 && likeCount}
              </span>
            }

          </div>
        </div>

        {
          isSelf && <MessageFeatures messageId={id} onDelete={onDelete} onEdit={onEdit} />
        }

      </div>
    </>
  );
}

export default Message;
