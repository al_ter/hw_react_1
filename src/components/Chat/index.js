import React, { useState, useEffect } from 'react';
import Spinner from '../Spinner';
import Header from '../Header';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import { MESSAGES_URL } from '../../config';
import { getUniquesByField, addLike, findById, atMoment, scrollToBottom, focusOnInput } from '../../helpers';
import { v4 as uuid } from 'uuid';

const Chat = () => {
  const [ messages, setMessages ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(false);
  const [ currentUserId, setCurrentUserId ] = useState(null); 
  const [ messageInEditor, setMessageInEditor ] = useState({});
  let lastMessage = messages[messages.length - 1];

  const onMessageSend = message => {
    if (message.id) {
      setMessages(messages.map(m => (
        message.id === m.id
          ? { ...message, editedAt: atMoment()}
          : m
      )));

      setMessageInEditor({});
    } else {
      setMessages([
        ...messages,
        {
          id: uuid(),
          userId: currentUserId,
          avatar: '',
          user: 'User',
          text: message.text,
          createdAt: atMoment(),
          editedAt: ''
        }
      ]);
    }
  };

  const onLike = messageId => {
    setMessages(messages.map(message => (
      message.id === messageId
        ? addLike(message)
        : message
    )));
  };

  const onDelete = messageId => {
    setMessages(messages.filter(message => message.id === messageId ? false : true));
  };

  const onEdit = messageId => {
    setMessageInEditor(findById(messages, messageId));
    focusOnInput();
  };

  useEffect(() => {
    const fetchMessages = async () => {
      setIsLoading(true);

      const response = await fetch(MESSAGES_URL);
      const messages = await response.json();

      setMessages(messages);
      setIsLoading(false);
    };
 
    fetchMessages();
  }, []);

  useEffect(() => {
    setCurrentUserId(uuid());
  }, []);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  return (
    isLoading
      ? <Spinner />
      : (
        <main className='content'>
          <div className='container chat'>
            <Header
              chatName='Chat name'
              usersCount={getUniquesByField(messages)}
              messagesCount={messages.length}
              messagesLastAt={lastMessage && lastMessage.createdAt}
            />
            <MessageList
              messages={messages}
              onLike={onLike}
              onDelete={onDelete}
              onEdit={onEdit}
              currentUserId={currentUserId}
            />
            <MessageInput
              currentUserId={currentUserId}
              onMessageSend={onMessageSend}
              message={messageInEditor}
            />
          </div>
        </main>
      )
  );
}

export default Chat;
