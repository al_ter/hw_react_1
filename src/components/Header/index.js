import React from 'react'

const Header = ({ chatName, usersCount, messagesCount, messagesLastAt }) => {
  const date = new Date(messagesLastAt);
  return (
    <div className='chat-header py-2 px-4 border-bottom'>
        <div className='header__chat-name h4'>
          {chatName}
        </div>
        <div className='header__users'>
          <span className='font-weight-bolder'>Users:</span> {usersCount}
        </div>
        <div className='header__messages-count'>
          <span className='font-weight-bolder'>Messages:</span> {messagesCount}
        </div>
        <div className='header__messages-last'>
          <span className='font-weight-bolder'>Last message:</span> {date.toLocaleString()}
        </div>
    </div>
  );
};

export default Header;
