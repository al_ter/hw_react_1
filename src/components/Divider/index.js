import React from 'react';

const Divider = ({ dividerTimestamp }) => {
  const date = new Date(dividerTimestamp);

  return (
    <div className="divider">
      <span>{date.toLocaleDateString()}</span>
    </div>
  );
};

export default Divider;
