import React, { useState, useEffect } from 'react';

const MessageInput = ({ message, onMessageSend }) => {
  const [ text, setText ] = useState('');
  
  useEffect(() => {
    if (message.id) {
      setText(message.text);
    }
  }, [message]);

  const handleOnInput = ({ target: { value } }) => setText(value);

  const handleOnSubmit = e => {
    e.preventDefault();

    if (!text.trim()) return;

    message.id
      ? onMessageSend({ ...message, text })
      : onMessageSend({ text });
    
      setText('');
  };

  return (
    <div className='message-input py-3 px-4 border-top'>
      <form
        id='message-form'
        className='input-group'
        onSubmit={handleOnSubmit}
      >
        <input
          type='text'
          className='form-control'
          placeholder='Type your message'
          onInput={handleOnInput}
          value={text}
        />
        <button className='btn btn-primary'>
          Send
        </button>
      </form>
    </div>
  );
}

export default MessageInput;
