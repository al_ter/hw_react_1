export const getUniquesByField = array => {
  if (!array.length) return;
  const unique = new Set();

  array.forEach(item => unique.add(item.userId));

  return unique.size; 
};

export const sortByDate = array => array.sort((a, b) => Date(a.createdAt) - Date(b.createdAt));

export const addLike = message => (
  message.likeCount
    ? { ...message, likeCount: message.likeCount + 1 }
    : { ...message, likeCount: 1 }
);

export const findById = (array, id) => array.find(item => item.id === id);

export const findIdxByField = (array, field, value) => array.findIndex(item => item[field] === value);

export const atMoment = () => new Date().toISOString();

export const scrollToBottom = () => window.scrollTo(0, document.body.scrollHeight);

export const focusOnInput = () => {
  const input = document.querySelector('#message-form input');
  input.focus();
};
